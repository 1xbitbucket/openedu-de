# Developer information

## Requirements

Most development tools are available inside the Docker containers, however we recommended:

 * git - *windows https://git-scm.com/download/win
 * drush - optional
 * composer - *Windows installation with https://getcomposer.org/Composer-Setup.exe and reference PHP.exe from http://windows.php.net/download/.

Next to the normal Drupal developer tools we will also require Docker and Docker-compose:

*  Mac - recommended:
 https://docs.docker.com/docker-for-mac/install/
* Mac (Virtualbox): https://docs.docker.com/toolbox/toolbox_install_mac/
* Windows - recommended:
 https://docs.docker.com/docker-for-windows/install/
* Windows (Virtualbox): https://docs.docker.com/toolbox/toolbox_install_windows/
* Unix: https://docs.docker.com/install/#docker-ce

In case necessary you would need to enable Virtualization in your BIOS settings. If dependencies are not met for Mac or Windows, install the DockerToolbox instead. This will run lightweight VM in Virtualbox. Open 'Docker Quickstart Terminal' on Windows to start Docker and to interact with the terminal.

## Starting OpenEDU
#### Clone the OpenEDU project.
```
git clone git@bitbucket.org:1xbitbucket/openedu-de.git OpenEDU
```

#### Install OpenEDU project with composer
```
cd OpenEDU
composer install --ignore-platform-reqs
```

#### Use docker-compose to start our infrastructure
```
docker-compose up -d
```
When using Docker Toolbox you would need to set the port in Kinematic  (nginx container -> Hostname/Ports -> Set a port -> Save). The IP will need to be used for accessing the containers on Windows (See https://prnt.sc/iqa8kp). You are also still able to adjust CPU/Memory usage by opening up Virualbox and changing the VM settings.

#### Default configuration for MySQL:
```
host: mariadb
database: drupal
username: drupal
password: drupal
```

#### Export the current database:
```
docker-compose exec mariadb sh -c 'exec mysqldump --all-databases -uroot -p"password"' > db/databases.sql
```
**Warning!**: docker containers will not keep your database state if you use  ``docker-compose down`` so it is important to export your database! Else you should use ``docker-compose stop``.

#### Import a database
In the same directory as the docker-compose.yml file you will find the db directory (or create it) and drop in your .sql .sql.gz file(s). 
All SQL files will be automatically imported once MariaDB container has started after running ``docker-compose up -d``.

